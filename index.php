<?php

error_reporting(E_ALL & ~E_NOTICE);

use LogProcessor\System\Application;

require_once(__DIR__ . '/System/Application.php');

$app = new Application();
$app->start();
$app->end();
