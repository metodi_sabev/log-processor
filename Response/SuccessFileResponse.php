<?php

namespace LogProcessor\Response;

/**
 * Class SuccessFileResponse
 *
 * @package LogProcessor\Response
 */
class SuccessFileResponse extends FileResponse
{
    const LOG_PATH = __DIR__ . '/../log/output/log-processor.json';

    public function __construct()
    {
        self::deleteExistingFiles();

        parent::__construct();
    }

    /**
     * Delete the file if it already exists.
     */
    private function deleteExistingFiles()
    {
        if (file_exists(self::LOG_PATH)) {
            unlink(self::LOG_PATH);
        }
    }
}
