<?php

namespace LogProcessor\Response;

use SplFileObject;

/**
 * Abstract class FileResponse
 *
 * @package LogProcessor\Response
 */
abstract class FileResponse
{
    /**
     * @var SplFileObject
     */
    protected $file;

    public function __construct()
    {
        $this->file = new SplFileObject(static::LOG_PATH, 'w+');
    }

    /**
     * Write the given message to the log file.
     *
     * @param  string $message
     */
    public function log(string $message)
    {
        $this->file->fwrite($message . PHP_EOL);
    }
}
