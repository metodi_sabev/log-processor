<?php

namespace LogProcessor\Response;

/**
 * Class ErrorFileResponse
 *
 * @package LogProcessor\Response
 */
class ErrorFileResponse extends FileResponse
{
    const LOG_PATH = __DIR__ . '/../log/output/log-processor.log.err';
}
