<?php

namespace LogProcessor\Service;

use LimitIterator;
use LogProcessor\Entity\LogEntity;
use LogProcessor\Exception\CsvParserException;
use LogProcessor\Validator\LogValidator;
use LogProcessor\Response\ErrorFileResponse;
use SplFileObject;

/**
 * Abstract class LogHandler.
 *
 * @package LogProcessor\Service
 */
abstract class LogHandler
{
    /**
     * @var string
     */
    protected $filePath;
    
    /**
     * @var array
     */
    protected $result = [];

    /**
     * @var ErrorFileResponse
     */
    protected $errorLog;

    /**
     * Read the file and setup the SplFileObject with the desired configs.
     *
     * @return SplFileObject|null
     */
    abstract public function getFileObject(): ?LimitIterator;

    /**
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
        $this->errorLog = new ErrorFileResponse();
    }

    /**
     * Parse the file object
     */
    public function __invoke()
    {
        $limitIterator = $this->getFileObject() ?? [];

        foreach ($limitIterator as $line => $row) {
            try {
                $validator = new LogValidator($row);

                if ($validator->validate() === false) {
                    throw new CsvParserException($validator->getLogMessage($line));
                }

                $log = $validator->getObject();

                $this->addToResult($log);
            } catch (CsvParserException $e) {
                $this->errorLog->log($e->getMessage());
            }
        }

        return $this->result;
    }

    /**
     * Init result array
     *
     * @param  LogEntity $log
     */
    private function addToResult(LogEntity $log)
    {
        $this->result[$log->getHost()][$log->getDate()][$log->getTime()][$log->getHttpStatus()][$log->getTcpStatusAction()]['requests'] += 1;
        $this->result[$log->getHost()][$log->getDate()][$log->getTime()][$log->getHttpStatus()][$log->getTcpStatusAction()]['bytes'] += $log->getBytes();
    }
}
