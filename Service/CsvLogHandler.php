<?php

namespace LogProcessor\Service;

use SplFileObject;
use LimitIterator;

/**
 * Class CsvLogHandler
 *
 * @package LogProcessor\Service
 */
class CsvLogHandler extends LogHandler
{
    const SKIP_FIRST_LINE = true;

    /**
     * @var LimitIterator
     */
    protected $file;

    /**
     * Read the file and setup the SplFileObject with the desired configs.
     *
     * @return SplFileObject|null
     */
    public function getFileObject(): ?LimitIterator
    {
        if ($this->file instanceof LimitIterator) {
            return $this->file;
        }

        try {
            $this->file = new SplFileObject($this->filePath, 'r');

            $this
                ->file
                ->setFlags(SplFileObject::SKIP_EMPTY | SplFileObject::READ_AHEAD | SplFileObject::DROP_NEW_LINE | SplFileObject::READ_CSV)
            ;

            $this
                ->file
                ->setCsvControl(' ')
            ;

            if (self::SKIP_FIRST_LINE) {
                $this->file = new LimitIterator($this->file, self::SKIP_FIRST_LINE);
            }
        } catch (\RuntimeException $e) {
            print('File not found');

            return null;
        }

        return $this->file;
    }
}
