<?php

namespace LogProcessor\Controller;

use LogProcessor\Response\SuccessFileResponse;
use LogProcessor\Service\CsvLogHandler;

/**
 * Class CsvLogController
 *
 * @package LogProcessor\Controller
 */
class CsvLogController
{
    const DEFAULT_FILE_PATH = __DIR__ . '/../log/input/log-processor.log';

    /**
     * @param string|null $filePath
     */
    public function __construct(?string $filePath)
    {
        $this->filePath = $filePath ?? self::DEFAULT_FILE_PATH;
    }

    public function __invoke(): void
    {
        $response = (new CsvLogHandler($this->filePath))();

        if ($response) {
            (new SuccessFileResponse())->log(json_encode($response));
        }
    }
}
