<?php

namespace LogProcessor\System;

/**
 * Class Application.
 *
 * @package LogProcessor\System
 */
class Application
{
    const START_MESSAGE = 'Starting .... ';
    const END_MESSAGE   = 'Ended in %s seconds';

    const CSV_LOG_TYPE  = 'csv';
    const LOG_HANDLERS  = [
        self::CSV_LOG_TYPE => \LogProcessor\Controller\CsvLogController::class,
    ];

    /**
     * @var float
     */
    private $startTime;

    /**
     * @var float
     */
    private $endTime;

    public function __construct()
    {
        $this->registerAutoloader();
    }

    /**
     * Register Autloader
     *
     * @param  string $namespace
     *
     * @return boolean
     */
    private static function autoloader($namespace): bool
    {
        $namespace = str_replace('LogProcessor\\', '', $namespace);
        $fileName = realpath(dirname(__FILE__)) . '/../' . str_replace('\\', '/', $namespace) . '.php';
        $className = explode('\\', $namespace);
        $className = $className[count($className)-1];

        if (file_exists($fileName)) {
            require_once($fileName);
            if (class_exists($className)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Register Autoloader
     */
    private function registerAutoloader()
    {
        spl_autoload_register('self::autoloader');
    }

    /**
     * Start the application.
     */
    public function start()
    {
        if (php_sapi_name() !== 'cli') {
            return false;
        }

        $this->print(self::START_MESSAGE);
        $this->startTime = microtime(true);

        list($logType, $filePath) = $this->getInputOptions();

        if (array_key_exists($logType, self::LOG_HANDLERS)) {
            $className = self::LOG_HANDLERS[$logType];

            return (new $className($filePath))();
        }

        return false;
    }

    /**
     * End the application.
     */
    public function end()
    {
        $this->endTime = microtime(true);

        $this->print(sprintf(self::END_MESSAGE, $this->getExecutionTime()));
    }

    /**
     * Calculate the execution time in seconds.
     *
     * @return float
     */
    public function getExecutionTime(): float
    {
        return $this->endTime - $this->startTime;
    }

    /**
     * Print a message to the console.
     *
     * @param  string $message
     */
    public function print(string $message)
    {
        print($message . PHP_EOL);
    }

    /**
     * Get console input options. Supported options are -t for type and
     * -f for file path
     *
     * @return array
     */
    private function getInputOptions(): array
    {
        $options = getopt("f:hp:t:hp");

        return [
            $options['t'] ?? self::CSV_LOG_TYPE,
            $options['f'] ?? null,
        ];
    }
}
