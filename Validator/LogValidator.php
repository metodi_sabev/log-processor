<?php

namespace LogProcessor\Validator;

use LogProcessor\Entity\LogEntity;

/**
 * Class LogValidator
 *
 * @package LogProcessor\Validator
 */
class LogValidator
{
    const FIELDS_COUNT_ERROR_MESSAGE    = 'Expecting %s fields but received %s on line %s';
    const MISSING_HTTP_ERROR_MESSAGE    = 'Missing HTTP Status on line %s';
    const MISSING_CACHE_ERROR_MESSAGE   = 'Missing cache status in %s on line %s';
    const INVALID_CACHE_ERROR_MESSAGE   = 'Invalid cache status in %s on line %s';
    const MISSING_HOST_ERROR_MESSAGE    = 'Missing Host on line %s';

    /**
     * @var array
     */
    private $data;

    /**
     * @var LogEntity
     */
    private $object;

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var array
     */
    private $values = [];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get LogEntity object.
     *
     * @return LogEntity
     */
    public function getObject(): LogEntity
    {
        return $this->object;
    }

    /**
     * Get the error message.
     *
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->message;
    }

    /**
     * Get the error values.
     *
     * @return array
     */
    public function getErrorValues(): array
    {
        return $this->values;
    }

    /**
     * Get a log message combining the error message and the error values.
     *
     * @param  int $line
     *
     * @return string
     */
    public function getLogMessage($line): string
    {
        $values = $this->getErrorValues();
        array_push($values, $line + 1);

        return vsprintf($this->getErrorMessage(), $values);
    }

    /**
     * Check if the TCP status is valud
     *
     * @param  string  $tcpStatus
     *
     * @return boolean
     */
    public function isValidTcpStatus($tcpStatus): bool
    {
        return strpos($tcpStatus, LogEntity::TCP_HIT) !== false ? true : (strpos($tcpStatus, LogEntity::TCP_MISS) !== false ? true : false);
    }

    /**
     * Validate the data
     *
     * @return bool
     */
    public function validate(): bool
    {
        $log = new LogEntity($this->data);

        if ($log->getLength() != count(LogEntity::ELEMENTS)) {
            $this->message = self::FIELDS_COUNT_ERROR_MESSAGE;
            $this->values = [count(LogEntity::ELEMENTS), $log->getLength()];

            return false;
        } elseif ($log->getHttpStatus() === false) {
            $this->message = self::MISSING_HTTP_ERROR_MESSAGE;

            return false;
        } elseif ($log->getTcpStatus() === false) {
            $this->message = self::MISSING_CACHE_ERROR_MESSAGE;

            return false;
        } elseif ($log->getHost() === false) {
            $this->message = self::MISSING_HOST_ERROR_MESSAGE;

            return false;
        } elseif (!$this->isValidTcpStatus($log->getTcpStatus())) {
            $this->message = self::INVALID_CACHE_ERROR_MESSAGE;
            $this->values = [$log->getTcpStatus()];

            return false;
        }

        $this->object = $log;

        return true;
    }
}
