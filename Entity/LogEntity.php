<?php

namespace LogProcessor\Entity;

/**
 * Class LogEntity
 *
 * @package LogProcessor\Entity
 */
class LogEntity
{
    const DATE          = 'date';
    const TIME          = 'time';
    const TIME_TAKEN    = 'time-taken';
    const C_IP          = 'c-ip';
    const FILESIZE      = 'filesize';
    const S_IP          = 's-ip';
    const S_PORT        = 's-port';
    const SC_STATUS     = 'sc-status';
    const SC_BYTES      = 'sc-bytes';
    const CS_METHOD     = 'cs-method';
    const CS_URI_STEM   = 'cs-uri-stem';
    const DELIMITER     = '-';
    const RS_DURATION   = 'rs-duration';
    const RS_BYTES      = 'rs-bytes';
    const C_REFERRER    = 'c-referrer';
    const C_USER_AGENT  = 'c-user-agent';
    const CUSTOMER_ID   = 'customer-id';
    const X_EC_CUSTOM   = 'x-ec_custom-1';

    const ELEMENTS = [
        self::DATE,
        self::TIME,
        self::TIME_TAKEN,
        self::C_IP,
        self::FILESIZE,
        self::S_IP,
        self::S_PORT,
        self::SC_STATUS,
        self::SC_BYTES,
        self::CS_METHOD,
        self::CS_URI_STEM,
        self::DELIMITER,
        self::RS_DURATION,
        self::RS_BYTES,
        self::C_REFERRER,
        self::C_USER_AGENT,
        self::CUSTOMER_ID,
        self::X_EC_CUSTOM,
    ];

    const TCP_HIT   = 'HIT';
    const TCP_MISS  = 'MISS';

    /**
     * @var array
     */
    private $record;

    /**
     * @param array $record
     */
    public function __construct(array $record)
    {
        $this->record = $record;
    }

    /**
     * Get a record's element
     *
     * @param  string $element
     *
     * @return string
     */
    private function getElement(string $element): string
    {
        return $this->record[array_search($element, self::ELEMENTS)];
    }

    /**
     * Get the record length
     *
     * @return int
     */
    public function getLength(): int
    {
        return count($this->record);
    }

    /**
     * Get the date formated in Y-m-d
     *
     * @return string
     */
    public function getDate(): string
    {
        $dateTime = new \DateTime($this->getElement(self::DATE));

        return $dateTime->format('Y-m-d');
    }

    /**
     * Get the time formated in H:i and aggregated by 10-minute period.
     *
     * @return string
     */
    public function getTime(): string
    {
        $dateTime = new \DateTime($this->getElement(self::TIME));

        $minutes = $dateTime->format('i');
        if ($minutes % 10 != 0) {
            $dateTime->sub(new \DateInterval('PT'.($minutes % 10).'M'));
        }

        return $dateTime->format('H:i');
    }

    /**
     * Get sc status
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->getElement(self::SC_STATUS);
    }

    /**
     * Get HTTP Status.
     *
     * @return string
     */
    public function getHttpStatus(): string
    {
        $status = explode('/', $this->getStatus());
        $httpStatus = $status[1] ?? null;

        return $httpStatus;
    }

    /**
     * Get TCP Status
     *
     * @return string|null
     */
    public function getTcpStatus(): ?string
    {
        $status = explode('/', $this->getStatus());

        return $status[0] ?? null;
    }

    /**
     * Get TCP Status' action.
     *
     * @return string|null
     */
    public function getTcpStatusAction(): ?string
    {
        $tcpStatus = explode('_', $this->getTcpStatus());

        return strtolower($tcpStatus[count($tcpStatus) -1]) ?? null;
    }

    /**
     * Get the host name.
     *
     * @return string|null
     */
    public function getHost(): ?string
    {
        $url = $this->getElement(self::CS_URI_STEM);

        return parse_url($url)['host'] ?? null;
    }

    /**
     * Get the bytes amount.
     *
     * @return string
     */
    public function getBytes(): string
    {
        return $this->getElement(self::SC_BYTES);
    }
}
